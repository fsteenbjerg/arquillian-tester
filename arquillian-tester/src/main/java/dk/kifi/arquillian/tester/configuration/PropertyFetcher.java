package dk.kifi.arquillian.tester.configuration;

import java.util.Properties;

public interface PropertyFetcher {
	Properties fetch(Class<? extends Configuration> clazz);
}
