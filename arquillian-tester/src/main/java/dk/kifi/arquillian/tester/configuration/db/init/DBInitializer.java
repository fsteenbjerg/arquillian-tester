package dk.kifi.arquillian.tester.configuration.db.init;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import dk.kifi.arquillian.tester.configuration.db.Category;
import dk.kifi.arquillian.tester.configuration.db.Property;
import dk.kifi.arquillian.tester.configuration.sample.SampleConfiguration;

@Singleton
@Startup
@LocalBean
public class DBInitializer {
	@PersistenceContext EntityManager em;
	private static final String DEMO_USER = "jb0278";
	private static final String GOOGLE_URL = "http://plus.google.com";
	private static final String FACEBOOK_URL = "http://facebook.com";

	@PostConstruct
	public void init() {
		Category c = new Category(SampleConfiguration.class.getName());
		add(c, SampleConfiguration.DEMO_USER_KEY, DEMO_USER);
		add(c, SampleConfiguration.FACEBOOK_URL_KEY, FACEBOOK_URL);
		add(c, SampleConfiguration.GOOGLE_URL_KEY, GOOGLE_URL);
		em.persist(c);
	}
	
	protected void add(Category c, String key, String value)  {
		Property p = new Property(key, value);
		p.setCategory(c);
		c.getProperties().add(p);
	}
}
