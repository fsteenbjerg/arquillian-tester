package dk.kifi.arquillian.tester.configuration.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.function.Consumer;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Category {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 1, max = 200)
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="category")
    private Collection<Property> properties = new ArrayList<>();
   
	public Category() {
	}

	public Category(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Property> getProperties() {
		return properties;
	}

	public void setProperties(Collection<Property> properties) {
		this.properties = properties;
	}
	
	public Properties asProperties() {
		Properties result = new Properties();
		getProperties().stream().forEach(consumeProperties(result));
		return result;
	}

	public Consumer<Property> consumeProperties(Properties props) {
		return p -> props.put(p.getName(), p.getValue());
	}
	
	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + ", properties=" + properties + "]";
	}
}