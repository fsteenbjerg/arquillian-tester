package dk.kifi.arquillian.tester.configuration.db;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Property {
    @Id
    @GeneratedValue
    private Long id;
    
    private LocalDateTime lastUpdated;

    @ManyToOne 
    private Category category;

    @NotNull
    @Size(min = 1, max = 200)
    private String name;
    
    @NotNull
    @Size(min = 1, max = 200)
	private String value;
   
	public Property() {
	}
	
	public Property(String name, String value) {
		this.name = name;
		this.value = value;
		lastUpdated = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString() {
		return "Property [id=" + id + ", lastUpdated=" + lastUpdated + ", name=" + name + ", value=" + value + "]";
	}
}
