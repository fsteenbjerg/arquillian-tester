package dk.kifi.arquillian.tester.configuration;

import java.util.Properties;

import javax.annotation.PostConstruct;

public abstract class Configuration {
	private Properties properties = null;
	
	@PostConstruct
	public void init() {
		properties = getPropertyFetcher().fetch(this.getClass());
	}
	
	protected abstract PropertyFetcher getPropertyFetcher();
	
	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	public String getProperty(String key) {
		return properties.getProperty(key);
	}

	@Override
	public String toString() {
		return "Configuration [properties=" + properties + "]";
	}
}
