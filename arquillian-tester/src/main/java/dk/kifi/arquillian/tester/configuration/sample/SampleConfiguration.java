package dk.kifi.arquillian.tester.configuration.sample;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import dk.kifi.arquillian.tester.configuration.Configuration;
import dk.kifi.arquillian.tester.configuration.PropertyFetcher;

@ApplicationScoped
public class SampleConfiguration extends Configuration {
	public static final String GOOGLE_URL_KEY = "google.url";
	public static final String FACEBOOK_URL_KEY = "facebook.url";
	public static final String DEMO_USER_KEY = "demo.user";

	@Inject BeanManager bm;

	@SuppressWarnings("cdi-ambiguous-dependency")
	@Inject PropertyFetcher fetcher;

	@Override
	protected PropertyFetcher getPropertyFetcher() {
		return fetcher;		
	}
	
	@PostConstruct
	public void init() {
		super.init();
	}
	
	@Produces
	@Config("")
	public URL getURL(InjectionPoint ip) {
		String urlKey = "";
		try {
			urlKey = ip.getAnnotated().getAnnotation(Config.class).value();
			return new URL(getProperty(urlKey));
		} catch (MalformedURLException e) {
			throw new RuntimeException("Failed to construct url with key " + urlKey, e);
		}
	}

	@Produces
	@Config("")
	public String getConfigurationProperty(InjectionPoint ip) {
		String key = ip.getAnnotated().getAnnotation(Config.class).value();
		return getProperty(key);
	}

}
