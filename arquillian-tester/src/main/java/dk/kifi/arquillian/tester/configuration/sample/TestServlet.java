package dk.kifi.arquillian.tester.configuration.sample;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("test")
public class TestServlet extends HttpServlet {
	@Inject @Config(SampleConfiguration.GOOGLE_URL_KEY) URL googleUrl;
	@Inject @Config(SampleConfiguration.FACEBOOK_URL_KEY) URL facebookUrl;
	@Inject @Config(SampleConfiguration.DEMO_USER_KEY) String demoUser;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try (PrintWriter out = response.getWriter()) {
			out.append("Google Url: " + googleUrl + "\n")
			   .append("Facebook Url: " + facebookUrl + "\n")
			   .append("Demo user: " + demoUser + "\n");
		}
	}
}
