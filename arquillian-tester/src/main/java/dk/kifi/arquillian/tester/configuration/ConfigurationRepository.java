package dk.kifi.arquillian.tester.configuration;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

@ApplicationScoped
public class ConfigurationRepository {
	@Inject BeanManager bm;
	protected final Map<Class<? extends Configuration>, Configuration> loadedConfigurations = new HashMap<>();

	public <T extends Configuration> T getConfiguration(final Class<T> configurationClass) {
		Configuration configuration = loadedConfigurations.get(configurationClass);
		if (configuration == null) {
			@SuppressWarnings("unchecked")
			Bean<T> bean = (Bean<T>) bm.getBeans(configurationClass).iterator().next();
			CreationalContext<T> ctx = bm.createCreationalContext(bean);
			configuration = configurationClass.cast(bm.getReference(bean, configurationClass, ctx));
			loadedConfigurations.put(configurationClass, configuration);			
		}
		return configurationClass.cast(configuration);
	}
	
	public Collection<Configuration> getAllLoadedConfigurations() {
		return loadedConfigurations.values();
	}
	
}
