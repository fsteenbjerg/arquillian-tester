package dk.kifi.arquillian.tester.configuration.db;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import dk.kifi.arquillian.tester.configuration.Configuration;
import dk.kifi.arquillian.tester.configuration.PropertyFetcher;

public class DatabaseFetcher implements PropertyFetcher {
	@PersistenceContext 
	private EntityManager em;
	
	public DatabaseFetcher() {
	}
	
	@Override
	public Properties fetch(Class<? extends Configuration> configurationClass) {
		CriteriaBuilder cb = em.getCriteriaBuilder();

    	Class<Category> clazz = Category.class;
    	CriteriaQuery<Category> cq = cb.createQuery(clazz);
    	Root<Category> from = cq.from(clazz);

    	cq.select(from).where(cb.equal(from.get("name"), configurationClass.getName()));
    	
    	TypedQuery<Category> q = em.createQuery(cq);
    	Category category = q.getSingleResult();
 
    	return category.asProperties();
	}
}
