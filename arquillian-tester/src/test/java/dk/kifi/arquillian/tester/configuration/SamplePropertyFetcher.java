package dk.kifi.arquillian.tester.configuration;

import java.util.Properties;

import javax.enterprise.inject.Alternative;

import dk.kifi.arquillian.tester.configuration.sample.SampleConfiguration;

@Alternative
public class SamplePropertyFetcher implements PropertyFetcher {
	public static final String DEMO_USER = "Fin Steenbjerg";

	@Override
	public Properties fetch(Class<? extends Configuration> configurationClass) {
		Properties props = new Properties();
		props.setProperty(SampleConfiguration.DEMO_USER_KEY, DEMO_USER);
		return props;
	}
}
