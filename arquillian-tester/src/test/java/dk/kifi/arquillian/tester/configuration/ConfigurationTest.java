package dk.kifi.arquillian.tester.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.kifi.arquillian.tester.configuration.sample.Config;
import dk.kifi.arquillian.tester.configuration.sample.SampleConfiguration;

@RunWith(Arquillian.class)
public class ConfigurationTest {
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationTest.class);

	@Deployment
	public static Archive<?> createDeployment() {
		WebArchive archive = ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackage(Configuration.class.getPackage())
				.addPackage(SampleConfiguration.class.getPackage())
				.addPackage(ConfigurationTest.class.getPackage())
				.addAsWebInfResource("test-beans.xml", "beans.xml");
		logger.debug("Confent of archive: {}", archive.toString(true));
		return archive;
	}

	@Inject SampleConfiguration config;
	@Inject @Config(SampleConfiguration.DEMO_USER_KEY) String demoUser;
	@Inject ConfigurationRepository repo;
	
	@Test
	public void testThatConfigurationIsWorking() {
		assertNotNull("Configuration not injected", config);
		assertEquals(SamplePropertyFetcher.DEMO_USER, config.getProperty(SampleConfiguration.DEMO_USER_KEY));
	}

	@Test
	public void testThatConfigurationPropertyInjectionIsWorking() {
		assertNotNull("Demo user not injected", demoUser);
		assertEquals(SamplePropertyFetcher.DEMO_USER, demoUser);
	}

	@Test
	public void testThatConfigurationRepositoryIsWorking() {
		assertNotNull("Repository not injected", repo);
		SampleConfiguration configuration = repo.getConfiguration(SampleConfiguration.class);
		assertNotNull("Configuration not found", configuration);
		logger.info("Sample Configuration : {}", configuration);
		assertEquals(SamplePropertyFetcher.DEMO_USER, configuration.getProperty(SampleConfiguration.DEMO_USER_KEY));
	}
}
